# Roleplays of REGE

## REGEdt32
Start time: 2020-03-03  
Deprecation time: 2023-08-19  
REGEdt32, which is formerly named “regedit” and has a Chinese version “注册表编辑器”.
### Settings
* Prefers Chinese Language
* Interested in OSes especially Windows

## RheniumGermanide
Start time: 2022-11-21  
Deprecation time: 2027-01-11  
RheniumGermanide, which has a case-sensitive alias named “ReGe”.
### Settings
* Prefers English Language
* Interested in chemistry

## RegE_x
Start time: 2023-11-21  
Deprecation time: 2027-01-21  
### Settings
* Prefers programming-language-form communication
* Interested in programming

## RegularEpression
Start time: 2024-01-05  
Deprecation time: 2027-08-21
### Settings
* Prefers English language
* Interested in physics
